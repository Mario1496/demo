package com.example.demo.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Tarea;
import com.example.demo.repository.TareaRepository;

@RestController
@Validated
@RequestMapping("/auth/tarea")
public class TareaController {

	
	@Autowired
	private TareaRepository tareaRepository;

	
	//Se listan todas las tareas que se encuentren en la BD
	@GetMapping("/listar")
	List<Tarea> listarTareas() {
		return tareaRepository.findAll();
	}

	//Se crea la tarea para lo cual se necesita la estructura de la clase Tarea
	@PostMapping("/crear")
	Tarea crearTarea(@RequestBody Tarea tarea) {
		return tareaRepository.save(tarea);
	}


//Se actualiza la tarea X, para lo cual necesitamos la ID de la tarea y el estado que puede ser 0 (True) o 1 (True)
	@PutMapping("/cambiarEstado/{idTarea}/{estadoTarea}")
	ResponseEntity<Tarea> actualizarTarea(@PathVariable long idTarea, @PathVariable int estadoTarea) {

		Optional<Tarea> tarea = tareaRepository.findById(idTarea);

		if (tarea.isPresent()) {

			if (estadoTarea == 0) {

				tarea.get().setEstado(true);

			} else if (estadoTarea == 1){

				tarea.get().setEstado(false);
			}

			tareaRepository.save(tarea.get());

			return ResponseEntity.ok(tarea.get());

		}else {
			return ResponseEntity.notFound().build();
		}

		

	}

}
