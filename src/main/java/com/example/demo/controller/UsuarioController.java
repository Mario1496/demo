package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Usuario;
import com.example.demo.repository.PerfilRepository;
import com.example.demo.repository.UsuarioRepository;
import com.example.demo.request.LoginForm;
import com.example.demo.response.JwtResponse;
import com.example.demo.security.jwt.JwtProvider;

@RestController
@Validated
@RequestMapping("/auth/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	PerfilRepository perfilRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	// Se listan todos los usuarios que se encuentra en la BD
	@GetMapping("/listar")
	List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}

	// Se crea un usuario el cual requiere la estructura de la clase Usuario, la
	// contraseña se encripta por temas de seguridad, ademas se valida que el
	// username no exista
	@PostMapping("/crear")
	public ResponseEntity<Usuario> crearUsuario(@RequestBody Usuario usuario) {

		if (usuarioRepository.existsByUsername(usuario.getUsername())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		} else {

			usuario.setPassword(new BCryptPasswordEncoder().encode(usuario.getPassword()));

			usuarioRepository.save(usuario);

			return new ResponseEntity<>(usuario, HttpStatus.CREATED);

		}

	}

	// Login de Usuario
	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@RequestBody LoginForm loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();

		return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));

	}

}
