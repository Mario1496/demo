package com.example.demo.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.model.Perfil;
import com.example.demo.model.PerfilName;

public interface PerfilRepository extends JpaRepository<Perfil, Long> {

	Optional<Perfil> findByNombre(PerfilName perfilName);

}
