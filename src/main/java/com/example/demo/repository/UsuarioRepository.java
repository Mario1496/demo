package com.example.demo.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.model.Usuario;

//Se realiza la extension de las clases con JPA, por defecto esta trae una gran cantidad de funcionalidades como el Crud, por lo que no sera necesario crear una query personalizada o algo similiar
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Boolean existsByUsername(String username);

	Optional<Usuario> findByUsername(String username);

}
